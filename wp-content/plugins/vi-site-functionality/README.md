# Vector & Ink Site Functionality Plugin
This plugin is a base plugin to get theme development started. It should contain all code that would stick around if we were to do a redesign (custom post type, admin customizations, etc.).

## Installation ##
1. `git clone https://bitbucket.org/adlit/adl-site-functionality` in the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Add new files to the `/includes` directory and load them from `adl-site-functionality.php`

### Customize theme
1. Replace `::client name::` in `adl-site-functionality.php` with the site name
2. Replace Vector & Ink logo with site logo (`assets/login.png`)
2. Customize login styles (`assets/css/vi-admin-login.css`)

### Plugin Code
Include everything that wouldn't change if we were going to redesign the site.
- Admin customizations
- Registering custom post types & taxonomies
- Functions for modifying custom post types & taxonomies

### Theme Code
Don't include this stuff in this plugin. Theme specific code should go in the theme.
- Enqueueing scripts & styles
- Defining widgets
- Defining menus

# Features
- Easily modify login page with custom logo & styles (edit `assets/css/vi-admin-login.css`)
- Push YoastSEO meta box to the bottom of all pages

## Custom Post Types and Taxonomies

This plugin includes post-types/ and taxonomies/ folders. Use WP-CLI to generate custom post types & taxonomies scripts to occupy these folders from the command line:

```
wp scaffold post-type my-post-type --label="My Post Type" --plugin=adl-site-functionality
wp scaffold taxonomy my-taxonomy --label="My Taxonomy" --plugin=adl-site-functionality
```

These commands will generate:
- .../plugins/adl-site-functionality/post-type/my-post-type.php
- .../plugins/adl-site-functionality/taxonomies/my-taxonomy.php


The commands can be made even simpler by adding a global WP-CLI config file at ~/.wp-cli/config.yml:

```
scaffold post-type:
  plugin: adl-site-functionality
scaffold taxonomy:
  plugin: adl-site-functionality
```

(An example file can be found here: https://bitbucket.org/snippets/adlit/nBpGy)
(More documentation on global parameters: https://make.wordpress.org/cli/handbook/config/#global-parameters)


After adding this you can leave off the --plugin parameter from the command, and WP-CLI will use the global default:

wp scaffold post-type my-post-type --label="My Post Type"
wp scaffold taxonomy my-taxonomy --label="My Taxonomy"

### Notes:
- Remember to add these new files to the $includes array in the plugin bootstrap file!
- In order to use "Archive Settings" which is added by this plugin, add a 3rd parameter of '1' to any custom post type init actions. For instance, if you've added a custom post type of 'hamster-car', edit the post-types/hamster-car.php file as such:

```
add_action( 'init', 'hamster_car_init', 1 );
```
