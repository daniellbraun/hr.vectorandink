<?php

function photo_gallery_init() {
	register_post_type( 'photo-gallery', array(
		'labels'                => array(
			'name'               => __( 'Photo Galleries', 'adl-site-functionality' ),
			'singular_name'      => __( 'Photo Gallery', 'adl-site-functionality' ),
			'all_items'          => __( 'All Photo Galleries', 'adl-site-functionality' ),
			'new_item'           => __( 'New Photo Gallery', 'adl-site-functionality' ),
			'add_new'            => __( 'Add New', 'adl-site-functionality' ),
			'add_new_item'       => __( 'Add New Photo Gallery', 'adl-site-functionality' ),
			'edit_item'          => __( 'Edit Photo Gallery', 'adl-site-functionality' ),
			'view_item'          => __( 'View Photo Gallery', 'adl-site-functionality' ),
			'search_items'       => __( 'Search Photo Galleries', 'adl-site-functionality' ),
			'not_found'          => __( 'No Photo Galleries found', 'adl-site-functionality' ),
			'not_found_in_trash' => __( 'No Photo Galleries found in trash', 'adl-site-functionality' ),
			'parent_item_colon'  => __( 'Parent Photo Gallery', 'adl-site-functionality' ),
			'menu_name'          => __( 'Photo Galleries', 'adl-site-functionality' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'revisions' ),
		'has_archive'           => true,
		'rewrite'               => array(
			'with_front' => false,
			'slug' => 'misc/photo-gallery'
		),
		'query_var'             => true,
		'menu_icon'             => 'dashicons-format-gallery',
		'show_in_rest'          => true,
		'rest_base'             => 'photo-gallery',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}

add_action( 'init', 'photo_gallery_init', 1 );

function photo_gallery_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['photo-gallery'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( __( 'Photo Gallery updated. <a target="_blank" href="%s">View Photo Gallery</a>', 'adl-site-functionality' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'adl-site-functionality' ),
		3  => __( 'Custom field deleted.', 'adl-site-functionality' ),
		4  => __( 'Photo Gallery updated.', 'adl-site-functionality' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Photo Gallery restored to revision from %s', 'adl-site-functionality' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __( 'Photo Gallery published. <a href="%s">View Photo Gallery</a>', 'adl-site-functionality' ), esc_url( $permalink ) ),
		7  => __( 'Photo Gallery saved.', 'adl-site-functionality' ),
		8  => sprintf( __( 'Photo Gallery submitted. <a target="_blank" href="%s">Preview Photo Gallery</a>', 'adl-site-functionality' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9  => sprintf( __( 'Photo Gallery scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Photo Gallery</a>', 'adl-site-functionality' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __( 'Photo Gallery draft updated. <a target="_blank" href="%s">Preview Photo Gallery</a>', 'adl-site-functionality' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}

add_filter( 'post_updated_messages', 'photo_gallery_updated_messages' );
