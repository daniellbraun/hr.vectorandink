<?php
/**
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

namespace VectorAndInk;

use WP_Rewrite;

add_action( 'generate_rewrite_rules', __NAMESPACE__ . '\\taxonomy_slug_rewrite' );


/**
 * Replace Taxonomy slug with Post Type slug in URL. Allows you to use same slug for
 * a post type and custom taxonomy.
 *
 * NOTE: This means that a post type and a taxonomy CANNOT have the same slug, or one of them
 * won't be viewable!
 *
 * @link http://someweblog.com/wordpress-custom-taxonomy-with-same-slug-as-custom-post-type/
 *
 * @param WP_Rewrite $wp_rewrite Rewrite instance.
 */
function taxonomy_slug_rewrite( $wp_rewrite ) {
	$rules = array();

	// Get all custom post types & taxonomies.
	$taxonomies = get_taxonomies( array( '_builtin' => false ), 'objects' );
	$post_types = get_post_types( array( 'public' => true, '_builtin' => false ), 'objects' );

	foreach ( $post_types as $post_type ) {
		foreach ( $taxonomies as $taxonomy ) {

			// Go through all post types which this taxonomy is assigned to.
			foreach ( $taxonomy->object_type as $object_type ) {

				// Check if taxonomy is registered for this custom post type.
				if ( $object_type === $post_type->name ) {
					// Get category objects.
					$terms = get_categories( array(
						'type'       => $object_type,
						'taxonomy'   => $taxonomy->name,
						'hide_empty' => 0,
					) );

					// Make rewrite rules.
					foreach ( $terms as $term ) {
						$route           = $post_type->rewrite['slug'] . '/' . $term->slug . '/?$';
						$rules[ $route ] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
					}
				}
			}
		}
	}

	// Merge with global rewrite rules.
	$wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
