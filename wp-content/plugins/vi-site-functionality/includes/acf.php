<?php
/**
 * Register functions related to ACF.
 *
 * All functions using ACF hooks and functions should be defined here.
 *
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

namespace VectorAndInk;
